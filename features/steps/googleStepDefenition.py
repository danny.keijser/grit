from behave import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import time

chrome_options = Options()
# chrome_options.add_argument("--disable-extensions")
# chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("--no-sandbox") # linux only
chrome_options.add_argument("--headless=new") # for Chrome >= 109
# chrome_options.add_argument("--headless")
# chrome_options.headless = True # also works
driver = webdriver.Chrome(options=chrome_options)

@Given("I can reach google")
def i_can_reach_google(self,):
   print("Go to google..")
   
   #go to full screen
   driver.get("http://www.google.com")
   driver.set_page_load_timeout(30)

   print("max window..")
   driver.maximize_window()

   print("Click the cookies..")
   #click cookies
   #elem = driver.find_element(By.ID, "L2AGLb")
   #elem.click()
   driver.set_page_load_timeout(30)
   #normaal stop je geen sleep zonder reden in je code maar om de stappen goed te zien heb ik hem er hier in gezet.
   
   print("I see google in the title..")
   assert "Google" in driver.title

@When('I want to search for "{text}"')
def i_can_reach_google(self, text):
   
   elem = driver.find_element(By.ID, "APjFqb")
   elem.send_keys(text)
   time.sleep(3)
   #normaal stop je geen sleep zonder reden in je code maar om de stappen goed te zien heb ik hem er hier in gezet.
   elem.send_keys(Keys.RETURN)
   driver.set_page_load_timeout(30)


@when('I reach "{text}"')
def i_reach(self, text):
   print("Go to cucumber...")
   
   driver.get(text)
   driver.set_page_load_timeout(30)
   #normaal stop je geen sleep zonder reden in je code maar om de stappen goed te zien heb ik hem er hier in gezet.
   
   print("I see cucumber...")
   assert "Cucumber" in driver.title
   print("done..")
   print("done..")
